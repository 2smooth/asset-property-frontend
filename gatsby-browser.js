/**
 * Implement Gatsby's Browser APIs in this file.
 *
 * See: https://www.gatsbyjs.com/docs/browser-apis/
 */

// You can delete this file if you're not using it
import React from 'react'
import { Provider } from 'react-redux'

import createStore from './src/reducers/store'
import LayoutRedux from './src/components/layout'


export const wrapRootElement = ({ element }) => {

    return (
        <Provider store={createStore()}>
            <LayoutRedux>
                {element}
            </LayoutRedux>
        </Provider>
    )
    
}


export const shouldUpdateScroll = ({ prevRouterProps , routerProps  }) => {
    return true
}