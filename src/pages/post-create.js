import React, { useState } from "react"
import Layout from "../components/layout"

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import loginAction from "../reducers/login/actions"
import GraphAPI from '../services/graphql';

import { Upload, Modal, message, Button, Form, Input, DatePicker, InputNumber } from 'antd';
import { PlusOutlined, MinusCircleOutlined } from '@ant-design/icons';
import dayjs from "dayjs";
import format from 'dayjs';
import { navigateTo } from "gatsby";
import './style.css'
import styled from "styled-components";

const Container = styled.div`
    padding: 12px;
    padding-bottom: 20px;
    margin: 12px;
    border: 1px solid #efefef;
    border-radius: 10px;
    box-shadow: 1px 2px 20px 1px #eeeeee;
`

const PostCreatePage = () => {

    const saveImg = async (file) => {
        const {data} = await GraphAPI.uploadImgRequest(file.originFileObj)
        /*
        .then((res) => {
          console.log(res)
          const { postList } = res.data.data
        }).catch((e) => {
          console.log(e)
        })
        */
       console.log( data )
       return data.data.singleUpload.url
    }

    const createPostRequest = (post) => {
        let mock = {
            title : 'Test via web',
            description : 'Test via web',
            orderCloseAt : dayjs().format('YYYY-MM-DD HH:mm:ss'),
            deliveryAt : dayjs().format('YYYY-MM-DD HH:mm:ss'),
            deliveryLocation : 'Google Place',
            imageUrls : "https://www.planetware.com/wpimages/2020/02/france-in-pictures-beautiful-places-to-photograph-eiffel-tower.jpg, https://www.planetware.com/wpimages/2020/02/france-in-pictures-beautiful-places-to-photograph-mont-st-michel.jpg",
            postItems : [
                {
                    nameEn: 'Travel',
                    nameTh: 'Travel',
                    price: 300,
                    imageUrl: 'https://www.planetware.com/wpimages/2020/02/france-in-pictures-beautiful-places-to-photograph-versailles-gardens.jpg'
                }
            ]
        }

        GraphAPI.createPostRequest( post ).then((res) => {
          console.log(res)
          Modal.success({
            content: 'Create Post Success',
            onOk() { navigateTo(`/post-detail?id=${res.data.data.createPost.data.postId}`) },
          });
        }).catch((e) => {
          console.log(e)
          if( e.status === 400 ){
            const errorString = e.data.errors.map( error => ({ error: error.message }) )
            Modal.error({
                content: JSON.stringify(errorString, null,null, 2),
            })
          }
        })
    }

    const onFinish = async (values) => {
        console.log('Received values of form:', values);
        const { coverImgs, title, description, orderCloseAt, deliveryAt, deliveryLocation, items, deliveryPrice } = values
        const imageUrls = (await Promise.all( coverImgs.map( async img => await saveImg(img) ) )).join()
        const postItems = await Promise.all( items.map( async item => {
            let _item = {}
            _item.nameEn = item.name
            _item.nameTh = item.name
            _item.price = Number(item.price)
            _item.imageUrl = await saveImg(item.file.file)
            return _item
        }) )
        console.log( imageUrls, postItems )
        createPostRequest( { title, description, orderCloseAt: orderCloseAt.format('YYYY-MM-DD HH:mm:ss'), deliveryAt: deliveryAt.format('YYYY-MM-DD HH:mm:ss'), deliveryLocation, imageUrls, postItems, deliveryPrice } )
    };

    return (
        <>
            <Container>
                <h2>Create Post</h2>

                <PostForm
                    onFinish={onFinish}
                />
            </Container>
            <div style={{ padding: '8px 0' }}></div>
        </>
    )
}

const PostForm = ({ onChange, onFinish }) => {

    const [form] = Form.useForm();
    const [uploadCoverLimit, setUploadCoverLimit] = useState(false)

    const [fields, setFields] = useState([
        //{ name: ['title'], value: 'Ant Design' },
        //{ name: ['description'], value: 'Description' },
        { name: ['items'], value: [
            { name: '', price: '', file: undefined }
        ] },
        //{ name: ['date-time-picker'], value: dayjs() }
    ]);

    const config = {
        rules: [{ type: 'object', required: true, message: 'Please select time!' }],
        disabledDate: (current) => current && current < dayjs().endOf('day'),
        //disabledTime: (current) => current && current < dayjs().endOf('day')
    };

    const getBase64 = (img, callback) => {
        const reader = new FileReader();
        reader.addEventListener('load', 
        () => {
          callback(reader.result)
        });
        reader.readAsDataURL(img);
    }

    const UpdateProcess = (field) => {
        return {
            //name: 'file',
            listType: "picture-card",
            fileList: form.getFieldValue(field),
            //multiple: false,
            showUploadList: {
                showPreviewIcon: false,
                showRemoveIcon: true
            },
            customRequest: ({ onSuccess, onError, file }) => {
                onSuccess(null, file);
            },
            onChange(info) {
              if (info.file.status !== 'upcallingService') {
                console.log(info.file, info.fileList);
              }
              if (info.file.status === 'done' || info.file.status === 'error' || info.file.status === "removed") {
                console.log('info', info)
                getBase64(info.file.originFileObj, (imageUrl) => {
                    //message.success(`${info.file.name} file uploaded successfully`);
                    //updateImageProfile(imageUrl)
                    //setFileList(info.fileList)
                    form.setFieldsValue( {
                        [field]: info.fileList
                    } )

                    if( info.fileList.length >= 9 ){
                        setUploadCoverLimit(true)
                    }else{
                        setUploadCoverLimit(false)
                    }

                    console.log('form.getFieldValue(field)', form.getFieldValue(field))
                })
              }
            }
        };          
    }

    const uploadButton = (
        <div>
            <PlusOutlined />
            <div style={{ marginTop: 8 }}>Upload</div>
        </div>
    )

    const checkImgFileOfItem = ( f ) => {
        const items = form.getFieldValue( 'items' )
        //console.log( 'file', f.name, items, items[f.name] )
        //console.log( form.getFieldValue('coverImgs') )
        console.log( items, items[f.name] )
        return items[f.name] && items[f.name].file && items[f.name].file.fileList.length > 0
    }

    return(
        <Form
            form={form}
            name="global_state"
            fields={fields}
            /*
            onFieldsChange={(changedFields, allFields) => {
                console.log('onFieldsChange', allFields)
                onChange(allFields);
            }}
            */
            onFinish={onFinish}
        >
            <Form.Item
                name="coverImgs"
                style={{ marginBottom: '8px' }}
                shouldUpdate
                rules={[{ required: true, message: 'Image Cover is required!' }]}
                //shouldUpdate={(prevValues, curValues) => prevValues.fileList !== curValues.fileList}
                //rules={[{ required: true, message: 'Missing first name' }]}
            >   
                <Upload {...UpdateProcess('coverImgs')} multiple={true}>
                    { !uploadCoverLimit && uploadButton }
                </Upload>
            </Form.Item>


            <Form.Item
                name="title"
                label="Title"
                rules={[{ required: true, message: 'Title is required!' }]}
            >
                <Input />
            </Form.Item>

            <Form.Item
                name="description"
                label="Description"
                rules={[{ required: true, message: 'Description is required!' }]}
            >
                <Input.TextArea />
            </Form.Item>

            <Form.Item name="orderCloseAt" label="orderCloseAt" {...config}>
               
                <DatePicker 
                    /*showTime={{ format: 'HH:mm' }} format="YYYY-MM-DD HH:mm" placeholder="Select Time"*/
                    //showTime 
                    showTime={{format: 'HH:mm' }}
                    format="YYYY-MM-DD HH:mm"
                    placeholder="Select Time" 
                    style={{ width: '100%' }}
                />

            </Form.Item>

            <Form.Item name="deliveryAt" label="deliveryAt" {...config}>
               
                <DatePicker 
                    /*showTime={{ format: 'HH:mm' }} format="YYYY-MM-DD HH:mm" placeholder="Select Time"*/
                    //showTime 
                    showTime={{format: 'HH:mm' }}
                    format="YYYY-MM-DD HH:mm"
                    placeholder="Select Time" 
                    style={{ width: '100%' }}
                />

            </Form.Item>

            <Form.Item name="deliveryLocation" label="deliveryLocation" rules={[{ required: true, message: 'Delivery Location is required!' }]}>   
                <Input.TextArea />
            </Form.Item>

            <Form.Item
                name="deliveryPrice"
                label="Delivery Price"
                rules={[{ required: true, message: 'Delivery Price is required!' }]}
            >
                <InputNumber style={{ width: '100%' }} />
            </Form.Item>

            <hr />
            <h3>Items </h3>

            <div className={'product-items'}>
                <Form.List name="items" >
                {(fields, { add, remove }) => (
                <>
                    {fields.map(field => (
                        <div key={'_post_item_'+field.key} style={{ display: 'flex', marginBottom: 8 }} align="baseline">
                            <Form.Item
                                {...field}
                                name={[field.name, 'file']}
                                fieldKey={[field.fieldKey, 'file']}
                                style={{ marginBottom: '8px', textAlign: 'center' }}
                                rules={[{ required: true, message: 'Product Image is required!' }]}
                                //rules={[{ required: true, message: 'Missing first name' }]}
                            >   
                                {
         
                                    <Upload {...UpdateProcess(field.fieldKey)} >
                                        { !checkImgFileOfItem( field ) && uploadButton }
                                    </Upload>
                                }
                            </Form.Item>
                            
                            <div>
                                <Form.Item
                                    {...field}
                                    name={[field.name, 'name']}
                                    fieldKey={[field.fieldKey, 'name']}
                                    //rules={[{ required: true, message: 'Missing last name' }]}
                                    style={{ marginBottom: '8px' }}
                                    rules={[{ required: true, message: 'Product Name is required!' }]}
                                >
                                    <Input.TextArea placeholder="Item Name" />
                                </Form.Item>
                                <Form.Item
                                    {...field}
                                    name={[field.name, 'price']}
                                    fieldKey={[field.fieldKey, 'price']}
                                    style={{ marginBottom: '8px' }}
                                    rules={[{ required: true, message: 'Product Price is required!' }]}
                                    //rules={[{ required: true, message: 'Missing last name' }]}
                                >
                                    <Input placeholder="Price" />
                                </Form.Item>
                            </div>
                            { 
                                fields.length > 1 && 
                                <MinusCircleOutlined style={{ marginLeft: '4px', fontSize: '20px', color: 'red' }} onClick={() => remove(field.name)} />
                            }

                        </div>
                    ))}

                    <Form.Item>
                        <Button type="dashed" onClick={() => add()} block icon={<PlusOutlined />} style={{ color: 'green', borderColor: 'green' }}>
                            Add Items
                        </Button>
                    </Form.Item>
                </>
                )}
            </Form.List>
            </div>

            <hr style={{ color: '#efefef', marginBottom: '20px' }} />

            <Button htmlType="submit" block>
                Create Post
            </Button>

        </Form>
    )
}


function mapStateToProps(state) {
    return {

    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({

    }, dispatch)
}

let PostCreatePageRedux = connect(
    mapStateToProps,
    mapDispatchToProps
)(PostCreatePage);

export default PostCreatePageRedux
