import React from "react"
import Layout from "../components/layout"

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Upload, message, Button, Modal } from 'antd';
import { LoadingOutlined, PlusOutlined } from '@ant-design/icons';
import { useState } from "react";
import styled from "styled-components";
import GraphAPI from "../services/graphql";

const UploadStyle = styled( Upload )`

    &.ant-upload-picture-card-wrapper{
        text-align: center;
        min-height: 200px;
        height: 100%;
        display: table;
    }

    & .ant-upload.ant-upload-select-picture-card {
        width: 100%;
        height: 100%;
        max-width: 50vw;
        max-height: 50vh;
    }
`

function getBase64(img, callback) {
    const reader = new FileReader();
    reader.addEventListener('load', () => callback(reader.result));
    reader.readAsDataURL(img);
  }
  
  function beforeUpload(file) {
    const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
    if (!isJpgOrPng) {
      message.error('You can only upload JPG/PNG file!');
    }
    /*
    const isLt2M = file.size / 1024 / 1024 < 2;
    if (!isLt2M) {
      message.error('Image must smaller than 2MB!');
    }
    */
    return isJpgOrPng //&& isLt2M;
  }

export const PaymentContainer = (props) => {

  const [loading, setLoading] = useState(false)
  const [img, setImg] = useState(undefined)
  const [file, setFile] = useState(undefined)

  const handleChange = info => {
      if (info.file.status !== 'upcallingService') {
          console.log(info.file, info.fileList);
        }
      if (info.file.status === 'uploading') {
          setLoading( true );
        return;
      }
      if (info.file.status === 'done' || info.file.status === 'error') {
        // Get this url from response in real world.
        getBase64(info.file.originFileObj, imageUrl => {
            setLoading( true )
            setImg(imageUrl)
            setFile(info.file)
          }
        );
      } else if (info.file.status === 'error') {
          //message.error(`${info.file.name} file upload failed.`);
        }

    };

  const uploadButton = (
    <div>
      {loading ? <LoadingOutlined /> : <PlusOutlined />}
      <div style={{ marginTop: 8 }}>Upload</div>
    </div>
  );

  const saveImg = async (file) => {
  const {data} = await GraphAPI.uploadImgRequest(file.originFileObj)
   console.log( data )
   return data.data.singleUpload.url
}

  const uploadSlipProcess = async () => {
    const { orderId } = props
    const slipUrl = await saveImg(file)
    const {data} = await GraphAPI.uploadSlipRequest({
      orderId, slipUrl
    })
    console.log( 'uploadSlipProcess', data )

    const { orderUploadSlip } = data.data

    if( orderUploadSlip.status === 200 ){
      Modal.success({
        content: 'Upload Slip Completed.',
        onOk() { 
          setImg(undefined)
          setFile(undefined)
          if( props.processAfterUploadSlipComplete ){
            props.processAfterUploadSlipComplete()
          }
        },
      });
    }

  }

  return (
    <>
    <div style={{ margin: '16px' }}>
        <UploadStyle
            name="avatar"
            listType="picture-card"
            className="avatar-uploader"
            showUploadList={false}
            multiple={false}
            //action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
            beforeUpload={beforeUpload}
            onChange={handleChange}
        >
            {img ? <img src={img} alt="avatar" style={{ width: '100%' }} /> : uploadButton}
        </UploadStyle>
    </div>

    <div style={{ margin: '16px' }}>
        <Button 
          style={{ width: '100%' }}
          onClick={ uploadSlipProcess }
        >
            Upload Slip
        </Button>
    </div>
    </>
  )
}
  
const PostPaymentPage = () => {

    return (
        <>
            <div style={{ margin: '16px' }}>
                Order Detail + Slip Info
            </div>

            <hr />

            <PaymentContainer />
        </>
    )
}


function mapStateToProps(state) {
    return {

    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({

    }, dispatch)
}

let PostPaymentPageRedux = connect(
    mapStateToProps,
    mapDispatchToProps
)(PostPaymentPage);

export default PostPaymentPageRedux
