import React from "react"
import { useEffect } from "react"
import { UserManager } from "oidc-client";
import { config } from "../services/login-config";
import { navigate } from "gatsby";

const CallbackPage = () => {

    useEffect( () => {
        handleLogin()
    }, [])

    const handleLogin = async () => {
        let userManager = new UserManager(config);
        let user = await userManager.signinRedirectCallback();
        console.log( 'CallbackPage', user )
        localStorage.setItem('token', user.id_token)
        navigate('/home')
    }
    
    return (
        <></>
    )
}

export default CallbackPage
