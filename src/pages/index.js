import React, { useState, useEffect } from "react"
import { Link, navigate } from "gatsby"

import Container from "../components/layout"
import Image from "../components/image"
import SEO from "../components/seo"

import { Carousel, Layout, Menu, Input, Card, Avatar, Timeline, Button } from 'antd';
import { MailOutlined, AppstoreOutlined, SettingOutlined } from '@ant-design/icons';

import ImageGallery from 'react-image-gallery';
import "react-image-gallery/styles/css/image-gallery.css";

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import loginAction from "../reducers/login/actions"

const { Header, Footer, Sider, Content } = Layout;
const { SubMenu } = Menu;
const { Meta } = Card;

const contentStyle = {
  height: '160px',
  color: '#fff',
  lineHeight: '160px',
  textAlign: 'center',
  background: '#364d79',
};

const images = [
  {
    original: 'https://picsum.photos/id/1018/1000/600/',
    thumbnail: 'https://picsum.photos/id/1018/250/150/',
  },
  {
    original: 'https://picsum.photos/id/1015/1000/600/',
    thumbnail: 'https://picsum.photos/id/1015/250/150/',
  },
  {
    original: 'https://picsum.photos/id/1019/1000/600/',
    thumbnail: 'https://picsum.photos/id/1019/250/150/',
  },
];

const IndexPage = (props) => {

  useEffect(() => navigate('/home'),[])

  let current = 'mail'

  const onChange = (a, b, c) => {
    console.log(a, b, c);
  }

  const handleClick = e => {
    console.log('click ', e);
    this.setState({ current: e.key });
  };


  const createMenu = () => {
      return (<Menu onClick={handleClick} selectedKeys={[current]} mode="horizontal">
      <Menu.Item key="mail" icon={<MailOutlined />}>
        Navigation One
      </Menu.Item>
      <Menu.Item key="app" disabled icon={<AppstoreOutlined />}>
        Navigation Two
      </Menu.Item>
    </Menu>)
  }

  return (
    <Container>
      <>

        <Content style={{ minHeight: '85vh' }}>

          {/*
          <div style={{ height: '40vh' }}>
            <div style={{ width: '80%', margin: '0 auto', padding: '5vw 0 0'}}>
              <h2>Title</h2>
              <h3>Content......</h3>
              <Input addonAfter={<SettingOutlined />} defaultValue="mysite" />
            </div>
          </div>
          */}

          {/*
          <div style={{ minHeight: '40vh', padding: '2vw' }}>

            <h3>Service / Feature</h3>

            <div style={{ width: '80%', margin: '0 auto', textAlign: 'center'}}>
              <MailOutlined />
              <h3>Feature</h3>
              <div>Description</div>
            </div>

            <div style={{ width: '80%', margin: '0 auto', textAlign: 'center'}}>
              <MailOutlined />
              <h3>Feature</h3>
              <div>Description</div>
            </div>

            <div style={{ width: '80%', margin: '0 auto', textAlign: 'center'}}>
              <MailOutlined />
              <h3>Feature</h3>
              <div>Description</div>
            </div>

            <div style={{ width: '80%', margin: '0 auto', textAlign: 'center'}}>
              <MailOutlined />
              <h3>Feature</h3>
              <div>Description</div>
            </div>

            <div style={{ width: '80%', margin: '0 auto', textAlign: 'center'}}>
              <MailOutlined />
              <h3>Feature</h3>
              <div>Description</div>
            </div>

          </div>
          */}

          {/*
          <div style={{ minHeight: '40vh', padding: '2vw' }}>
            <h3>Step</h3>
            <div style={{ width: '80%', margin: '0 auto', padding: '5vw 0 0'}}>

              <Timeline>
                <Timeline.Item>Create a services site 2015-09-01</Timeline.Item>
                <Timeline.Item>Solve initial network problems 2015-09-01</Timeline.Item>
                <Timeline.Item>Technical testing 2015-09-01</Timeline.Item>
                <Timeline.Item>Network problems being solved 2015-09-01</Timeline.Item>
              </Timeline>

            </div>

          </div>
          */}

          {/*

          <div style={{ minHeight: '40vh', padding: '2vw', margin: '0 auto', }}>
            <h3>Feedback</h3>

            <Card
              style={{ width: 300, margin: '8px auto' }}
              cover={
                <img
                  alt="example"
                  src="https://picsum.photos/id/1015/1000/600/"
                />
              }
            >
              <Meta
                //avatar={<Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />}
                title="Card title"
                description="This is the description"
              />
            </Card>

            <Card
              style={{ width: 300, margin: '0 auto 8px' }}
              cover={
                <img
                  alt="example"
                  src="https://picsum.photos/id/1015/1000/600/"
                />
              }
            >
              <Meta
                //avatar={<Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />}
                title="Card title"
                description="This is the description"
              />
            </Card>

            <Card
              style={{ width: 300, margin: '0 auto 8px' }}
              cover={
                <img
                  alt="example"
                  src="https://picsum.photos/id/1015/1000/600/"
                />
              }
            >
              <Meta
                //avatar={<Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />}
                title="Card title"
                description="This is the description"
              />
            </Card>
          </div>

          */}

          <div style={{ minHeight: '40vh', padding: '2vw', margin: '0 auto', }}>
            <h3>Join Us or Try it</h3>
            <Button style={{ width: '100%' }}>Login</Button>
            <br/>
            <br/>
            <Button style={{ width: '100%' }} onClick={ () => navigate('/home') }>Try it</Button>
          </div>

          {/*
          <ImageGallery 
            items={ images.map( img => { return {original: img.original, thumbnail: img.thumbnail} } )} 
            showIndex={false}
            showPlayButton={false}
            infinite={false}
            showFullscreenButton={false}
            showNav={false}
            showThumbnails={false}
            showBullets={true}
          />
          */}

        </Content>
        <Footer style={{ background: '#61d195', paddingBottom: 0 }}>
          <h3>About</h3>
          <h3>Connect with us</h3>
        </Footer>
      </>
    </Container>
  )
}


function mapStateToProps(state) {
  return {

  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({

  }, dispatch)
}

let IndexPageRedux = connect(
  mapStateToProps,
  mapDispatchToProps
)(IndexPage);

export default IndexPageRedux
