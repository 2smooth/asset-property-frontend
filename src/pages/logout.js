import React from "react"
import { useEffect } from "react"
import { UserManager } from "oidc-client";
import { config } from "../services/login-config";
import { navigate } from "gatsby";

const LogoutPage = () => {

    useEffect( () => {
        navigate('/home')
    }, [])

    return (
        <></>
    )
}

export default LogoutPage
