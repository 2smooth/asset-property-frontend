import React, { useState, useEffect } from "react"
import { Link, push, navigate } from "gatsby"

import Container from "../components/layout"

import "react-image-gallery/styles/css/image-gallery.css";

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import loginAction from "../reducers/login/actions"
import Layout from "../components/layout";
import HomeMobilePageRedux from "../components/home/mobile";
import GraphAPI from '../services/graphql';

const HomePage = (props) => {

  const [postList, setPostList] = useState([])
  const [currentPage, setCurrentPage] = useState(0)
  const [totalPost, setTotalPost] = useState(1)
  const [loading, setLoading] = useState(false)

  useEffect(() => {
    loadPost()
  }, [])

  const loadPost = async () => {
    console.log('currentPage', currentPage)
    if( !loading && postList.length < totalPost ){
      setLoading(true)
      GraphAPI.getPostListRequest(currentPage)
      .then((res) => {
        console.log(res)
        const { postList: postListData } = res.data.data
        if( postList.length === 0 ){
          setPostList(postListData.edges)
        }else{
          setPostList( [...postList, ...postListData.edges] )
        }
        setCurrentPage(postListData.pageInfo.currentPage)
        setTotalPost(postListData.totalCount)
        setLoading(false)
      }).catch((e) => {
        console.log(e)
      })
    }
  }

  const goToPostDetail = (id) => {
    navigate(`/post-detail?id=${id}`)
  }

  return (
    <>
    <HomeMobilePageRedux loadPost={loadPost} currentPage={currentPage} postList={postList} goToPostDetail={goToPostDetail} />
    </>
  )
}


function mapStateToProps(state) {
  return {

  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({

  }, dispatch)
}

let HomePageRedux = connect(
  mapStateToProps,
  mapDispatchToProps
)(HomePage);

export default HomePageRedux
