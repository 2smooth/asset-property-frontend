import React, { useEffect } from "react"
import Layout from "../components/layout"

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import loginAction from "../reducers/login/actions"
import { Button, Modal } from "antd";
import { navigate, navigateTo } from "gatsby";
import { EditOutlined, PlusOutlined, MinusOutlined, CloseOutlined, CheckOutlined } from "@ant-design/icons";
import styled from "styled-components";
import { useState } from "react";
import GraphAPI from '../services/graphql';
import GoogleMapReact, { fitBounds } from 'google-map-react';
import topNavAction from "../reducers/topnav/actions";

const OrderNumberContainer = styled.div`
    width: 40px;
`

const OrderNumber = styled.div`
    border: 1px solid #e2e2e2;
    padding: 2px 8px;
    border-radius: 4px;
    width: fit-content;
    margin-right: 16px;
`

const OrderName = styled.div`
    flex-grow: 1;
`

const OrderPrice = styled.div`
    margin-right: 12px;
    color: #28a745;
`

const ContactCard = styled.div`
    border: 1px solid #efefef;
    border-radius: 5px;
    box-shadow: 1px 2px 20px 1px #eeeeee;
    padding: 8px 16px;
`

const Marker = (props) => {
    const { color, name, id } = props;
    return (
      <div className="marker"
        style={{ backgroundColor: color, cursor: 'pointer'}}
        title={name}
      />
    );
};

const OrderItem = ({ index=1, detail={}, AddToCart, removeFromCart }) => {

    const [isEdit, setIsEdit] = useState(false)

    return (
        <div style={{ display: 'flex', justifyContent: 'space-between', padding: '12px 0', borderBottom: '1px solid #f0f0f0', alignItems: 'center' }}>
            <OrderNumberContainer>
                <OrderNumber>{detail.quantity}</OrderNumber>
            </OrderNumberContainer>
            <OrderName>{detail.nameTh}</OrderName>
            <OrderPrice>{detail.price * detail.quantity} บาท</OrderPrice>
            { !isEdit && <div><EditOutlined onClick={ () => setIsEdit(true) }/></div> }
            { isEdit && 
                <div style={{ display: 'flex', alignItems: 'center' }}>
                    <Button style={{borderColor: 'red'}}><MinusOutlined style={{ color: 'red' }} onClick={() => removeFromCart(detail)} /></Button>
                    <span style={{ margin: '0 6px' }}/>
                    <Button style={{borderColor: 'green'}}><PlusOutlined style={{ color: 'green' }} onClick={() => AddToCart(detail)} /></Button>
                    <span style={{ margin: '0 6px' }}/>
                    <CheckOutlined onClick={ () => setIsEdit(false)} />
                </div> 
            }
        </div>
    )
}

const PostOrderPage = (props) => {

    const [ carts, AddCart ] = useState([])
    const [ name, setName ] = useState('GoHew Hungry')
    const [ phone, setPhone ] = useState('0617899870')
    const [ address, SetAddress ] = useState('')
    const [ addressRemark, SetAddressRemark ] = useState('')

    let [ location, setLocation ] = useState([])
    let [ map , setMap ] = useState(undefined)
    let [ maps, setMaps ] = useState(undefined)
    let [ mapDragEvent, setMapDragEvent ] = useState(undefined)
    let [ mapCenterEvent, setMapCenterEvent ] = useState(undefined)

    let [infoWindow, setInfoWindow] = useState(undefined)
    let [geocoder, setGeocoder] = useState(undefined)

    useEffect( () => {
        props.setFixedTopNavAll(true)
        AddCart([...props.cart.products])
        SetAddress( (( props.cart||{} ).post ||{}).deliveryLocation )
        return () => {
            props.setFixedTopNavAll(false)
        }
    }, [])

    useEffect( () => {
        processCurrentLocation()
    }, [infoWindow, geocoder])

    const sumProductquantity = () => {
        let UniqueProducts = new Set(carts)
        console.log( 'UniqueProducts', UniqueProducts )

        UniqueProducts = [...UniqueProducts].map( UniqueProduct => {
            let quantity = (carts.filter((product)=> UniqueProduct.postItemId === product.postItemId ) || []).length
            UniqueProduct.quantity = quantity
            return UniqueProduct
        })

        console.log( 'UniqueProducts', UniqueProducts )

        return UniqueProducts
    }

    const totalProductPrice = () => {
        const total = carts.reduce( (total, item) => item.price + total, 0 )
        return total
    }

    const AddToCarts = (product) => {
        AddCart( [...carts, product] )
    }

    const removeFromCarts = (product) => {
        const itemExist = carts.filter( itemInCart => product.postItemId === itemInCart.postItemId )
        if( itemExist ){
            let isRemove = false // remove only one
            const newCart = carts.filter( itemInCart => {
                if( product !== itemInCart || isRemove ){
                    return itemInCart
                }else{
                    isRemove = true
                }
            })

            if( itemExist.length === 1 ){
                Modal.confirm({
                    content: `Would you like to remove ${product.nameTh} from your order?`,
                    onOk: () => AddCart( [...newCart] ),
                })
            }else{
                AddCart( [...newCart] )
            }
        }
    }

    const createOrderProcess = async () => {
        const userLogin = localStorage.getItem('userId')

        if( userLogin ){
            doProcess()
        }else{
            props.setLoginModal(true)
        }

    }

    const doProcess = async () => {
        const { post } = props.cart

            const orderItems = sumProductquantity().map( order => {
                let _order = {}
                _order.postItemId = order.postItemId
                _order.quantity = order.quantity
                return _order
            })
    
            try{
                const {data} = await GraphAPI.createOrderRequest({ 
                    postId: post.postId, 
                    orderItems: orderItems,
                    deliveryLat: "0.0",
                    deliveryLng: "0.0",
                    userContactInfo: phone,
                    deliveryRemark: address,
                })
                const { createOrder } = data.data
    
                if( createOrder.status === 200 ){
                    const { orderId } = createOrder.data
                    Modal.success({
                        content: 'Create Order Success',
                        onOk() { navigate(`/my-order?order_id=${orderId}`) },
                    });
                }
            }catch(e){
                if( e.status === 400 ){
                    const errorString = e.data.errors.map( error => ({ error: error.message }) )
                    Modal.error({
                        content: JSON.stringify(errorString, null,null, 2),
                    })
                }
            }    
    }

    const backToOrderPage = () => {
        navigateTo(-1)
    }
    
    const apiIsLoaded = (map, maps) => {

        setMap(map)
        setMaps(maps)

        const center = map.getCenter();
        setLocation( [center.lat(), center.lng()] )

        const infoWindow = new maps.InfoWindow();
        const geocoder = new maps.Geocoder();

        setInfoWindow(infoWindow)
        setGeocoder(geocoder)

    }

    function handleLocationError(
        browserHasGeolocation,
        infoWindow,
        pos
      ) {
        infoWindow.setPosition(pos);
        infoWindow.setContent(
          browserHasGeolocation
            ? "Error: The Geolocation service failed."
            : "Error: Your browser doesn't support geolocation."
        );
        //infoWindow.open(map);
    }

    const customLocation = () => {
        infoWindow.close()
        const mapEvent = map.addListener('drag', function() {
            const center = map.getCenter();
            setLocation( [center.lat(), center.lng()] )
        });
        setMapDragEvent( mapEvent )

        const mapEvent1 = map.addListener('center_changed', function() {
            const center = map.getCenter();
            setLocation( [center.lat(), center.lng()] )
        });
        setMapCenterEvent(mapEvent1)
        
        const center = map.getCenter();
        setLocation( [center.lat(), center.lng()] )
    }

    const endCustomLocation = () => {
        maps.event.removeListener(mapDragEvent);
        maps.event.removeListener(mapCenterEvent);

        setMapDragEvent( undefined )

        const pos = {
            lat: location[0],
            lng: location[1],
        };

        processLocation(pos)
    }
          
    const processCurrentLocation = () => {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(
              (position) => {
                  const pos = {
                      lat: position.coords.latitude,
                      lng: position.coords.longitude,
                  };

                processLocation(pos)
      
              },
              () => {
                handleLocationError(true, infoWindow, map.getCenter());
              }
            );
          } else {
            // Browser doesn't support Geolocation
            handleLocationError(false, infoWindow, map.getCenter());
          }
    }

    const processLocation = (pos) => {
        console.log( 'geocoder', geocoder )
        if( geocoder )
            (geocoder).geocode({ location: pos }, (results, status) => {
                if (status === "OK") {

                    console.log( 'results ========>', results )
                    infoWindow.setContent( results[0].formatted_address );
                    SetAddress( results[0].formatted_address )
                }else{
                    infoWindow.setContent( 'Found Location' );
                }   

                setLocation( [pos.lat, pos.lng] )

                infoWindow.setPosition(pos);
                infoWindow.open(map);
                map.setCenter(pos);
                map.setZoom(18)

            })
    }

    return (
        <>
            <div style={{ margin: '16px' }}>
                <h3>Delivery Info</h3>
                <div>
                    <div style={{marginBottom: '8px'}}>Contact</div>
                    <ContactCard>
                        <div style={{ textAlign: 'right' }}><EditOutlined /></div>                            
                        <div> <span style={{fontWeight: 'bold'}}>ชื่อ:</span> {name} </div>
                        <div> <span style={{fontWeight: 'bold'}}>เบอร์โทร</span> {phone}</div>
                        <div> <span style={{fontWeight: 'bold'}}>สถานที่จัดส่ง:</span> {address}</div>
                        <div> <span style={{fontWeight: 'bold'}}>รายละเอียดเพิ่มเติม:</span> {addressRemark}</div>
                        <div style={{width: '100%', height: '300px'}}>
                            <GoogleMapReact
                                yesIWantToUseGoogleMapApiInternals
                                bootstrapURLKeys={{ 
                                    key: 'AIzaSyAGw658YwD8Ej0e5sNM7IYT1aVJhO18qQg',
                                    libraries:['places', 'geometry', 'drawing', 'visualization']
                                }}

                                onGoogleApiLoaded={({ map, maps }) => apiIsLoaded(map, maps)}

                                defaultCenter={[0, 0]}
                                defaultZoom={11}
                            >
                                <Marker
                                    lat={location[0]}
                                    lng={location[1]}
                                    name={"ตำแหน่งของคุณ"}
                                    color={"red"}
                                />
                            </GoogleMapReact>
                        </div>
                        {
                            !mapDragEvent &&
                            <Button type="primary" onClick={processCurrentLocation} style={{ backgroundColor: 'red', borderColor: 'red', width: '100%', margin: '16px 0' }} > เลือกตำแหน่งปัจจุบัน </Button>
                        }
                        {
                            !mapDragEvent ?
                            <Button type="primary" onClick={customLocation} style={{ backgroundColor: 'red', borderColor: 'red', width: '100%', marginBottom: '8px' }} > เลือกตำแหน่งเอง </Button>
                            :
                            <Button type="primary" onClick={endCustomLocation} style={{ backgroundColor: 'red', borderColor: 'red', width: '100%', margin: '16px 0' }} > ยืนยันตำแหน่ง </Button>
                        }
                    </ContactCard>
                </div>
            </div>

            <hr style={{
                height: '20px',
                backgroundColor: '#f0f0f0',
                border: 'none'
            }}/>

            <div style={{ margin: '16px', paddingBottom: '60px' }}>
                <h3>My Order</h3>
                <div>
                    {
                        sumProductquantity().map( product => {
                            return (
                                <OrderItem detail={product} 
                                    AddToCart={AddToCarts}
                                    removeFromCart={removeFromCarts}
                                />)
                        })
                    }
                </div>
                <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                    <div>ทั้งหมด</div>
                    <div> {totalProductPrice()} บาท</div>
                </div>
                <hr/>
                <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                    <div>ค่าจัดส่ง</div>
                    <div> {(props.post ||{}).deliveryPrice} บาท</div>
                </div>
                <hr/>
                <div style={{ display: 'flex', justifyContent: 'space-between', fontWeight: 'bold' }}>
                    <div>รวมทั้งสิ้น</div>
                    <div> {(props.post ||{}).deliveryPrice + totalProductPrice()} บาท</div>
                </div>
            </div>

            <div style={{ position: 'fixed', left: '16px', bottom: '16px', width: 'calc(100% - 32px)' }}>
                <Button 
                    style={{ width: '100%' }}
                    onClick={ () => carts.length === 0 ? backToOrderPage(): createOrderProcess()}
                >
                    { carts.length === 0 ? `Back To Order` : `Order Now` }
                </Button>
            </div>

        </>
    )
}


function mapStateToProps(state) {
    return {
        cart: state.cart,
        post: state.cart.post
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        setLoginModal: loginAction.functions.setLoginModal,
        setFixedTopNavAll: topNavAction.functions.setFixedTopNavAll
    }, dispatch)
}

let PostOrderPageRedux = connect(
    mapStateToProps,
    mapDispatchToProps
)(PostOrderPage);

export default PostOrderPageRedux
