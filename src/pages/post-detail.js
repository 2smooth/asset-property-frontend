import React from "react"
import Layout from "../components/layout"

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import loginAction from "../reducers/login/actions"
import { Avatar, Button, Badge } from "antd";
import { UserOutlined, PlusCircleOutlined, PlusOutlined, MinusOutlined, CalendarOutlined, SendOutlined, StarTwoTone, CheckCircleTwoTone } from "@ant-design/icons";
import { useState } from "react";
import { navigate } from "gatsby";
import { useEffect } from "react";
import queryString from 'query-string'

import GraphAPI from '../services/graphql';

//import "react-image-gallery/styles/css/image-gallery.css";
import ImageGallery from 'react-image-gallery';
import styled from "styled-components";

import "./post-detail.css";
import cartAction from "../reducers/cart/actions";
import dayjs from "dayjs";

const ImageGalleryStyle = styled(ImageGallery)`
`

const detail_list = [
    {
        id: '1',
        title: 'ปาท่องโก๋ 1 ชุด โก้จริงๆ',
        price: 50
    },
    {
        id: '2',
        title: 'ปาท่องโก๋ 2 ชุด โก้ใหญ่ขึ้น',
        price: 100
    },
    {
        id: '3',
        title: 'ปาท่องโก๋ 10 ชุด หรูโก้ๆ',
        price: 1000
    }
]

const MenuCard = ({ product = {}, AddCart, RemoveCart, inCart = 0 }) => {
    return (
        <div style={{ display: 'flex', alignItems: 'center', padding: '12px 0', borderBottom: '1px solid #e2e2e2' }}>
            <Badge count={inCart} showZero={false} >
                <img src={ product.imageUrl } style={{ width: '10vh', height: '10vh' }} />
            </Badge>
            <div style={{ marginLeft: '12px', flexGrow: '1' }}> 
                <div>{product.nameTh}</div>
                <div style={{ color: '#28a745' }}> {product.price} บาท</div>
            </div>
            {inCart > 0 && <MinusOutlined style={{ fontSize: '16px', marginRight: '12px' }} onClick={ () => RemoveCart(product) } />}
            <PlusOutlined style={{ fontSize: '16px' }} onClick={ () => AddCart(product)} />
        </div>
    )
}

const PostDetailPage = (props) => {

    const [ postDetail, setPostDetail ] = useState(undefined)
    const [ carts, AddCart ] = useState([])

    useEffect( () => {
        if (typeof window !== `undefined` && window.location) {
            const params = queryString.parse(window.location.search)
            GraphAPI.getPostByIdRequest( params.id )
            .then((res) => {
                console.log(res.data.data.post.edge)
                setPostDetail(res.data.data.post.edge)
            }).catch((e) => {
                console.log(e)
            })
        }
    }, [])

    const AddToCarts = (product) => {
        AddCart( [...carts, product] )
    }

    const removeFromCarts = (product) => {
        const itemEXist = carts.some( itemInCart => product.postItemId === itemInCart.postItemId )
        if( itemEXist ){
            let isRemove = false // remove only one
            const newCart = carts.filter( itemInCart => {
                if( product !== itemInCart || isRemove ){
                    return itemInCart
                }else{
                    isRemove = true
                }
            })
            AddCart( [...newCart] )
        }
    }

    const getTotalPrice = () => {
        return carts.reduce( (total, product) => product.price + total, 0 )
    }

    const getTotalInCartById = (postItemId) => {
        return (carts.filter( (cart) => cart.postItemId === postItemId ) || [] ).length
    }

    const goToPostOrderPage = () => {
        props.setPost(postDetail)
        props.setProductsToCart(carts)
        navigate('/post-order')
    }

    return (
        <>
            { postDetail &&
                <>
                <div style={{ margin: '16px', paddingBottom: '48px' }}>
                    <div style={{ margin: '8px 0', display: 'flex', alignItems: 'center' }}>
                        <Avatar size={32} icon={<img src={postDetail.user.profileImage} />} />
                        <div style={{display: 'flex', flexWrap: 'wrap'}}>
                            <div style={{ marginLeft: '8px', marginBottom: '0', flexBasis: '100%' }}>{postDetail.user.profileName}</div>
                            <div style={{ marginLeft: '8px', marginBottom: '0' }}>verified <CheckCircleTwoTone twoToneColor={'#16d0b5'} /></div>
                            <div style={{ marginLeft: '8px', marginBottom: '0' }}> 5.0 <StarTwoTone twoToneColor={'#f9a825'}/> </div>
                        </div>
                    </div>
                    <div>
                        <div style={{fontSize: '14px', fontWeight: 'bold'}}>{postDetail.titleTh}</div>
                        <div>{postDetail.descTh}</div>
                        <div style={{ display: 'flex', alignItems: 'center' }}>
                            <div style={{ marginRight: '8px', color: 'red' }}>ปิดรับ <CalendarOutlined style={{ marginLeft: '4px' }} /></div> { dayjs(postDetail.orderCloseAt).format('DD/MM/YYYY hh:mm') }
                        </div>
                        <div style={{ display: 'flex', alignItems: 'center' }}>
                            <div style={{ marginRight: '8px', color: 'blue' }}>จัดส่ง <CalendarOutlined style={{ marginLeft: '4px' }} /></div> { dayjs(postDetail.deliveryAt).format('DD/MM/YYYY hh:mm') }
                        </div>
                        <div style={{ display: 'flex', alignItems: 'center' }}>
                            <div style={{ marginRight: '8px', display: 'flex', alignItems: 'center',flex: 'none', alignSelf: 'baseline', color: 'blue' }}>จัดส่งที่ <SendOutlined style={{ marginLeft: '4px' }} /></div> <div>{ postDetail.deliveryLocation }</div>
                        </div>
                    </div>

                    <div className={'poster'} style={{ margin: '8px 0' }}>
                        <ImageGallery
                            items={ postDetail.imageUrls.map( img => { return {original: img, thumbnail: img} } )} 
                            showIndex={false}
                            showPlayButton={false}
                            infinite={true}
                            showFullscreenButton={false}
                            showNav={true}
                            showThumbnails={false}
                            showBullets={true}
                            //slideOnThumbnailOver={true}
                            autoPlay={true}
                            //disableThumbnailScroll={true}
                        />
                    </div>

                    <div>
                        <div>เมนู</div>
                        <div>
                            {
                                postDetail.postItems && postDetail.postItems.map( detail => <MenuCard product={detail} RemoveCart={removeFromCarts} AddCart={AddToCarts} inCart={ getTotalInCartById(detail.postItemId) }/>)
                            }
                        </div>
                    </div>

                </div>
                <div style={{ position: 'fixed', left: '16px', bottom: '16px', width: 'calc(100% - 32px)' }}>
                    <Button 
                        style={{ width: '100%' }}
                        onClick= { () => goToPostOrderPage() }
                        disabled={ carts.length === 0 }
                    > 
                        {carts.length || ''}  My Cart { carts.length > 0 ? getTotalPrice()+'$' : ''} 
                    </Button>
                </div>
            </>
            }
        </>
    )
}


function mapStateToProps(state) {
    return {

    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        setProductsToCart: cartAction.functions.setProductsToCart,
        setPost: cartAction.functions.setPost
    }, dispatch)
}

let PostDetailPageRedux = connect(
    mapStateToProps,
    mapDispatchToProps
)(PostDetailPage);

export default PostDetailPageRedux
