import React, { useEffect } from "react"
import Layout from "../components/layout"

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import loginAction from "../reducers/login/actions"
import { Avatar, Button } from "antd";
import { UserOutlined } from "@ant-design/icons";
import queryString from 'query-string'
import { useState } from "react";

const UserLayout = (props) => {

    useEffect(() => {
        console.log( `props.user_profile`, props.user_profile )
    }, [])

    const LogoutProcess = () => {
        localStorage.clear()
        window.location = '/home'
    }

    return (
    <>
    <div style={{ margin: '16px' }}>
        <Avatar size={64} icon={ props.user_profile.profileImage ? <img src={props.user_profile.profileImage} /> : <UserOutlined />} />
        <div style={{ margin: '20px 0' }}>
            Rating
        </div>
        <div style={{ margin: '16px 0' }}>
            <div>Name : {props.user_profile.firstName +' '+ props.user_profile.lastName}</div>
            <div>Gender</div>
            <div>Phone</div>
            <div>Email</div>
            <div>Address</div>
            <div>Bio</div>
        </div>
    </div>

    <hr style={{
            height: '20px',
            backgroundColor: '#f0f0f0',
            border: 'none'
        }}/>
    
    <div style={{ margin: '16px' }}>
            <Button style={{width: '100%'}} onClick={LogoutProcess}> Log out </Button>
        </div>

        <hr style={{
            height: '20px',
            backgroundColor: '#f0f0f0',
            border: 'none'
        }}/>

        <div style={{ margin: '16px' }}>
            <div>
                <div style={{ margin: '20px 0' }}>
                    Your History
                </div>
            </div>
        </div>
    </>
    )
}

const VisitorLayout = (props) => {
    return (
    <div style={{ margin: '16px' }}>
        <div style={{ display: 'flex', alignItems: 'center', marginBottom: '8px' }}>
            <Avatar size={64} icon={<UserOutlined />} />
            <div style={{ marginLeft: '8px' }}>
                <div>Name</div>
                <div>Rating</div>
            </div>
        </div>
        <div style={{ display: 'flex', justifyContent: 'space-between' }}>
            <div>Phone</div>
            <div>Email</div>
        </div>
        <div>Address</div>
    </div>
    )
}


const ProfilePage = (props) => {

    const [visitorLayout, setVisitorLayout] = useState(false)

    useEffect( () => {
        let params = queryString.parse(window.location.search)
        if( params.id ){
            setVisitorLayout(true)
        }
    }, [])

    return (
        <>
            {
                visitorLayout ? <VisitorLayout {...props}/> : <UserLayout {...props}/>
            }
        </>
    )
}


function mapStateToProps(state) {
    return {
        user_profile: state.login.user_profile
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        
    }, dispatch)
}

let ProfilePageRedux = connect(
    mapStateToProps,
    mapDispatchToProps
)(ProfilePage);

export default ProfilePageRedux
