import React, { useState, useEffect } from "react"
import { Link, push, navigate } from "gatsby"

import "react-image-gallery/styles/css/image-gallery.css";

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import loginAction from "../reducers/login/actions"
import Layout from "../components/layout";
import { Input, Button } from "antd";
import styled from "styled-components";
import { FilterOutlined } from "@ant-design/icons";
import { SearchResultCard } from "../components/search/utils/search-result-card";

const Container = styled.div`
  margin: 16px 16px 0;
  padding: 16px 0;
`

const { Search } = Input;

const SearchPage = (props) => {

  const onSearch = value => console.log(value)

  const goToPostDetail = (id) => {
    navigate(`/post-detail?id=${id}`)
  }

  return (
    <>
      <Container>
        <div style={{ display: 'flex' }}>
          <Search  placeholder="input search text" allowClear onSearch={onSearch} />
          <div style={{ whiteSpace: 'nowrap', marginLeft: '8px' }}>
            <Button>
              <FilterOutlined />
              ตัวกรอง
            </Button>
          </div>
        </div>
        <div style={{ margin: '16px 0' }}>
          <SearchResultCard goToPostDetail={goToPostDetail}/>
          <hr/>
          <SearchResultCard goToPostDetail={goToPostDetail}/>
          <hr/>
          <SearchResultCard goToPostDetail={goToPostDetail}/>
          <hr/>
          <SearchResultCard goToPostDetail={goToPostDetail}/>
          <hr/>
          <SearchResultCard goToPostDetail={goToPostDetail}/>
        </div>
      </Container>
    </>
  )
}


function mapStateToProps(state) {
  return {

  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({

  }, dispatch)
}

let SearchPageRedux = connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchPage);

export default SearchPageRedux
