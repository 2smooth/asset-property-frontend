import React, { useState, useEffect } from "react"
import { Link } from "gatsby"

import "react-image-gallery/styles/css/image-gallery.css";

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import loginAction from "../reducers/login/actions"
import LayoutRedux from "../components/layout";
import styled from 'styled-components'
import { Button, Badge, Modal } from "antd";
import GraphAPI from '../services/graphql';
import { UploadSlipModal } from "../components/utils/UploadSlipModal";
import { PictureOutlined } from "@ant-design/icons";
import queryString from 'query-string'

import { globalHistory } from "@reach/router/lib/history"
import topNavAction from "../reducers/topnav/actions";
import { useRef } from "react";

const Container = styled.div`
  margin: 16px;
`

const DetailCard = (props) => {

  const [slipModal, setSlipModal] = useState(false)
  const [img, setImg] = useState(false)

  const userId = localStorage.getItem('userId')
  const item_list = props.orderItems
  
  const getTotal = () => {
    return item_list.reduce( (total, item) => (item.price * item.quantity) + total, 0 )
  }

  const openSlipProcess = (url) => {
    setSlipModal(true)
    setImg(url)
  }

  return (
    <>
      <div id={props.orderId} style={{ padding: '12px 16px', borderBottom: '1px solid #f2f2f2'}}>
        <div style={{ display: 'flex' }}>
          <img src={props.post.imageUrls[0]} style={{ width: '15vh', height: '15vh', marginRight: '8px' }} />
          <div style={{ flexGrow: '1' }}>
            <div style={{ display: 'flex', justifyContent: 'space-between' }}>
              <h3 style={{ wordBreak: 'break-all' }}>{props.post.titleTh}</h3>
              <div>{props.confirmStatus}</div>
            </div>
            {
              item_list.map( item => {
                return (
                <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                  <div> {item.postItemNameTh} x {item.quantity} </div>
                  <div style={{ whiteSpace: 'nowrap' }}> {item.quantity * item.price} บาท </div>
                </div>
                )
              } )
            }
            <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
              <div> ทั้งหมด {getTotal()} บาท </div>
            </div>
          </div>
        </div>
        {
          props.confirmStatus === 'DELIVERYING' &&
          <div style={{ textAlign: 'right', color: '#ff02c6' }}>
            shippingCode : {props.shippingCode}
          </div>
        }
        { props.confirmStatus === 'WAITING' && props.userId === userId && 
        <div style={{ textAlign: 'right' }}>
          <Button onClick={ () => props.openSlipModal(props) }>Upload Slip</Button>
        </div>
        }
        { props.confirmStatus === 'PENDING' &&
        <div style={{ textAlign: 'right' }}>
          <div style={{ marginBottom: '12px' }}>
            Slip : <PictureOutlined onClick={ () => openSlipProcess(props.slipUrl) } />
          </div>
          {
            props.postUserId === userId && 
            <Button onClick={ () => props.confirmOrder(props.orderId) }>Confirm</Button>
          }
        </div>
        }
        { props.confirmStatus === 'CONFIRMED' && props.postUserId === userId && 
        <div style={{ textAlign: 'right' }}>
          <Button onClick={ () => props.confirmOrderDelivery(props.orderId) }>Delivery</Button>
        </div>
        }
      </div>
      <Modal 
        title="Slip Detail" 
        visible={slipModal} 
        onOk={ () => setSlipModal(false) } 
        onCancel={ () => setSlipModal(false) }
        centered={true}
        cancelButtonProps={{ style: { display: 'none' } }}
      >
        <img src={img} width="100%" height="100%"/>
      </Modal>
    </>
  )
}

const MyOrderPage = (props) => {

  const [currentTab, setCurrentTab] = useState('WAITING')
  const [orderList, setOrderList] = useState([])
  /*
  const [waitingList, setWaitingList] = useState([])
  const [deliveryList, setDeliveryList] = useState([])
  const [receiveList, setReceiveList] = useState([])
  */
  const [totalWaiting , setTotalWaiting] = useState(0)
  const [totalPending , setTotalPending] = useState(0)
  const [totalConfirmed , setTotalConfirmed] = useState(0)
  const [totalDeliverying , setTotalDeliverying] = useState(0)
  const [totalCompleted , setRotalCompleted] = useState(0)
  const [totalCanceled , setTotalCanceled] = useState(0)

  const [orderSelected, setOrderSelected] = useState(undefined)
  const [event, setEvent] = useState(undefined)

  const [currentPage, setCurrentPage] = useState(0)
  const [totalCount, setTotalCount] = useState(0)
  const [load, setLoad] = useState(false)

  const divScroll = useRef();

  useEffect( ()=> {
    
    //console.log( 'globalHistory', globalHistory)
    //globalHistory.listen( ({ location }) => {
      //console.log(location) 
      //checkOrderSelector()
    //});

    fetch()  

    props.setFixedTopNavAll(true)

    return () => {
      props.setFixedTopNavAll(false)
    }

  }, [])

  useEffect( () => {
    fetch()
  }, [currentTab])

  useEffect( () => {
    if( load && orderList.length < totalCount){
      fetch( currentPage )
    }
  }, [load])

  const handleScroll = (event) => {
    //console.log( event.target.scrollTop, event.target.scrollHeight, event.target.innerHeight )
    const scrollTop = (event.target && event.target.scrollTop) || document.body.scrollTop;
    const scrollHeight = (event.target && event.target.scrollHeight) || document.body.scrollHeight;
    if (scrollTop + window.innerHeight + 100 >= scrollHeight){
      setLoad(true);
      //console.log('Load', scrollTop, window.innerHeight, scrollHeight)
    }
  }

  /*
  useEffect( ()=> {
    checkOrderSelector()
  }, [ typeof window !== `undefined` && queryString.parse(window.location.search.order_id) ])
  */

  const mappingStatus = () => {

  }

  const fetch = async (currentPage) => {
    const {data} = await GraphAPI.getUserOrderRequest({ confirmStatus: currentTab, offset: currentPage })
    const { myOrderList } = data.data
    //const waitingList = myOrderList.edges.filter( order => (order.confirmStatus === 'WAITING') || (order.confirmStatus === 'PENDING') ) || [] // && order.postUserId !== userId
    //const deliveryList = myOrderList.edges.filter( order => (order.confirmStatus === 'CONFIRMED') ) || []
    //const receiveList = myOrderList.edges.filter( order => (order.confirmStatus === 'DELIVERYING') ) || []
    console.log( 'getUserOrderRequest', myOrderList.edge )
    if( currentPage ){
      setOrderList( [ ...orderList, ...myOrderList.edges ])
    }else{
      console.log( divScroll.current.scrollTop )
      divScroll.current.scrollTop = 0
      setOrderList(myOrderList.edges)
    }
    setCurrentPage(myOrderList.pageInfo.currentPage)
    //setDeliveryList(deliveryList)
    //setReceiveList(receiveList)
    setTotalWaiting(myOrderList.totalWaiting)
    setTotalPending(myOrderList.totalPending)
    setTotalConfirmed(myOrderList.totalConfirmed)
    setTotalDeliverying(myOrderList.totalDeliverying)
    setRotalCompleted(myOrderList.totalCompleted)
    setTotalCanceled(myOrderList.totalCanceled)

    setTotalCount(myOrderList.totalCount)
    setLoad(false)
    //checkOrderSelector(myOrderList)
  }
  /*
  const checkOrderSelector = () => {
    if (typeof window !== `undefined` && window.location) {
      const params = queryString.parse(window.location.search)

      console.log( params.order_id )

      if( params.order_id ){
        document.getElementById(params.order_id).scrollIntoView();
        document.getElementById(params.order_id).style.backgroundColor = '#e6e6e6'
        setTimeout( () => document.getElementById(params.order_id).style.backgroundColor = 'unset' , 5000)
      }
    }
  }
  */

  const openSlipModal = (order) => {
    setOrderSelected(order)
  }

  const closeSlipModal = () => {
    setOrderSelected(undefined)
  }

  const uploadSlipProcess = () => {

  }

  const processAfterUploadSlipComplete = () => {
    fetch()
    closeSlipModal()
  }

  const confirmOrder = async (orderId) => {
    console.log( 'orderId', orderId )
    const {data} = await GraphAPI.confirmSlipCorrectRequest({ orderId })
    console.log( data )
    const { orderShopConfirm } = data.data
    if( orderShopConfirm.status === 200 ){
      Modal.success({
        content: `Confirm Order(${orderId}) Completed.`,
        onOk() { 
          fetch()
        },
      });
    }
  }

  const confirmOrderDelivery = async (orderId) => {
    console.log( 'orderId', orderId )
    const {data} = await GraphAPI.confirmOrderDeliveryRequest({ orderId, shippingCode: '1234' })
    console.log( data )
    const { orderShopDelivery } = data.data
    if( orderShopDelivery.status === 200 ){
      Modal.success({
        content: `Delivery Order(${orderId}) Completed.`,
        onOk() { 
          fetch()
        },
      });
    }
  }

  const queryProduct = async () => {
    const listElm = this.$refs['product-container'];
    if(listElm.scrollTop + listElm.clientHeight >= listElm.scrollHeight && !this.loading) {
        await this.listProductFromCategory(this.currentTab, this.category_products[this.currentTab].length)
        let productListSelected = [...this.category_products[this.currentTab]]
        this.posts = [...productListSelected]
    }
  }

  return (
    <>
      <Container>
        <h2>การซื้อของฉัน</h2>
        <div style={{ overflowX: 'scroll', paddingTop: '8px', marginBottom: '24px' }}>
          <div style={{ display: 'flex', justifyContent: 'space-around', width: 'max-content' }}>
            <div onClick={ () => setCurrentTab('WAITING') } style={{ margin: '0 16px', borderBottom: currentTab === 'WAITING' ? '1px solid red' : '' }}>
              <Badge count={totalWaiting}><div style={{marginRight: '12px'}}>ที่ต้องชำระ</div></Badge>
            </div>
            <div onClick={ () => setCurrentTab('PENDING') } style={{ margin: '0 16px', borderBottom: currentTab === 'PENDING' ? '1px solid red' : '' }}>
              <Badge count={totalPending}><div style={{marginRight: '12px'}}>กำลังดำเนินการ</div></Badge>
            </div>
            <div onClick={ () => setCurrentTab('CONFIRMED') } style={{ margin: '0 16px', borderBottom: currentTab === 'CONFIRMED' ? '1px solid red' : '' }}>
              <Badge count={totalConfirmed}><div style={{marginRight: '12px'}}>ที่ต้องจัดส่ง</div></Badge>
            </div>
            <div onClick={ () => setCurrentTab('DELIVERYING') } style={{ margin: '0 16px', borderBottom: currentTab === 'DELIVERYING' ? '1px solid red' : '' }}>
              <Badge count={totalDeliverying}><div style={{marginRight: '12px'}}>ที่ต้องได้รับ</div></Badge>
            </div>
            <div onClick={ () => setCurrentTab('COMPLETED') } style={{ margin: '0 16px', borderBottom: currentTab === 'COMPLETED' ? '1px solid red' : '' }}>
              <Badge count={0}><div style={{marginRight: '12px'}}>ประวัติการสั่งซื้อ</div></Badge>
            </div>
          </div>
        </div>
      </Container>
        <div 
          ref = {divScroll}
          onScroll={handleScroll}
          style={{ overflow: 'scroll', height: 'calc( 100vh - 110px - 130px )' }}
        >
          {
            currentTab === 'WAITING' && orderList.map( (order,key) => <DetailCard key={key} {...order} openSlipModal={openSlipModal} confirmOrder={confirmOrder} /> )
          }
          {
            currentTab === 'PENDING' && orderList.map( (order,key) => <DetailCard key={key} {...order} confirmOrderDelivery={confirmOrderDelivery} /> )
          }
          {
            currentTab === 'CONFIRMED' && orderList.map( (order,key) => <DetailCard key={key} {...order} /> )
          }
          {
            currentTab === 'DELIVERYING' && orderList.map( (order,key) => <DetailCard key={key} {...order} /> )
          }
          {
            currentTab === 'COMPLETED' && orderList.map( (order,key) => <DetailCard key={key} {...order} /> )
          }
        </div>
      <UploadSlipModal 
        isModalVisible={orderSelected}
        handleOk={uploadSlipProcess} 
        handleCancel={closeSlipModal}
        order={orderSelected}
        processAfterUploadSlipComplete={processAfterUploadSlipComplete}
      />
    </>
  )
}


function mapStateToProps(state) {
  return {

  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    setFixedTopNavAll: topNavAction.functions.setFixedTopNavAll
  }, dispatch)
}

let MyOrderPageRedux = connect(
  mapStateToProps,
  mapDispatchToProps
)( MyOrderPage );

export default MyOrderPageRedux
