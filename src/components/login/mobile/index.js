import React, { useEffect, useState } from "react"
import { Modal } from "antd"
import { FacebookLoginButton } from "react-social-login-buttons";
import { navigateTo } from "gatsby";

export const LoginMobile = ({ 
    handleCancel, 
    handleOk,
    visible,
    login
}) => {

    const LoginWith5CE = () => {
        handleOk()
        localStorage.setItem('userId', '5ce26829-b785-4170-a3fd-08e8938fbae1')
    }

    const LoginWith9581 = () => {
        localStorage.setItem('userId', '9581c92b-4f3a-4378-9d45-e7d3b4f78f7c')
        handleOk()
    }

    return (
        <Modal
            title="Login"
            visible={visible}
            onOk={handleOk}
            onCancel={handleCancel}
            footer={null}
        >
            <FacebookLoginButton onClick={ LoginWith5CE } style={{ fontSize: '16px' }} text={'Login User 5CE'}/>
            <FacebookLoginButton onClick={ LoginWith9581 } style={{ fontSize: '16px' }} text={'Login User 9581'}/>
            <FacebookLoginButton onClick={ login } style={{ fontSize: '16px' }} text={'Login'}/>
      </Modal>
    )
}