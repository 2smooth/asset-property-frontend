import React, { useEffect, useState } from "react"
import styled from 'styled-components'
import { Avatar } from "antd"
import { UserOutlined, StarOutlined, StarTwoTone, CheckCircleTwoTone } from "@ant-design/icons"

const COntainer = styled.div`
    padding: 16px;
    margin: 16px;
    border: 1px solid #efefef;
    border-radius: 10px;
    box-shadow: 1px 2px 20px 1px #eeeeee;
`

const ImageDetail = styled.div`
    height: 60vh;
    background-color: #ec9c9c;
    overflow: hidden;
    position: relative;
    display: flex;
    flex-wrap: wrap;
    border-radius: 5px;
`

const Text = styled.div`
    //background-color: rgb(38 38 38 / 40%);
    //color: white;
    width: fit-content;
    margin: 4px 0;
    font-size: 12px;
    padding: 0 4px;
`

const ImgCover = styled.img`
    width: 100%;
    height: 100%;
    object-fit: cover;
`

const MoreImages = styled.div`
    position: absolute;
    color: white;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    background-color: #181818a6;
    display: flex;
    justify-content: center;
    align-content: center;
    align-items: center;
    font-size: 30px;
`

export const DetailCard = (props) => {
    
    const { detail={} } =  props
    const { user={} } = detail

    return (
        <COntainer onClick={ () => props.goToPostDetail(detail.postId) }>
            <div style={{ marginTop: '8px', display: 'flex', alignItems: 'center' }}>
                <Avatar size={32} icon={<img src={user.profileImage} />} />
                <div style={{display: 'flex', flexWrap: 'wrap'}}>
                    <div style={{ marginLeft: '8px', marginBottom: '0', flexBasis: '100%' }}>{user.profileName}</div>
                    <div style={{ marginLeft: '8px', marginBottom: '0' }}>verified <CheckCircleTwoTone twoToneColor={'#16d0b5'} /></div>
                    <div style={{ marginLeft: '8px', marginBottom: '0' }}> 5.0 <StarTwoTone twoToneColor={'#f9a825'}/> </div>
                </div>
            </div>
            <div style={{ textAlign: 'left' }}>
                <Text style={{fontSize: '14px', fontWeight: 'bold'}}>{detail.titleTh}</Text>
                <Text>{detail.descTh}</Text>
                <Text>ปิดรับ {detail.orderCloseAt}</Text>
                <Text>ส่ง {detail.deliveryAt}</Text>
                <Text>สถานที่ {detail.deliveryLocation}</Text>
            </div>
            <ImageComponent images={detail.imageUrls} />
        </COntainer>
    )
}

const ImageComponent = ({ 
    images=[]
}) => {

    const renderImg_1 = () => {
        return ( <ImageDetail style={{ height: '40vh' }}>
                <ImgCover src={images[0]} />
            </ImageDetail>)
    }

    const renderImg_2 = () => {
        return (<ImageDetail style={{ height: '45vh' }}>
                <div style={{ width: '50%', border: '0.1px solid white' }}>
                    <ImgCover src={images[0]}/>
                </div>
                <div style={{ width: '50%', border: '0.1px solid white' }}>
                    <ImgCover src={images[1]}/>
                </div>
            </ImageDetail>
        )
    }

    const renderImg_3 = () => {
        return (<ImageDetail>
            <div style={{ width: '100%', height: '55%', border: '0.1px solid white' }}>
                <ImgCover src={images[0]} />
            </div>
            <div style={{ width: '50%', height: '45%', border: '0.1px solid white' }}>
                <ImgCover src={images[1]}/>
            </div>
            <div style={{ width: '50%', height: '45%', border: '0.1px solid white' }}>
                <ImgCover src={images[2]}/>
            </div>
        </ImageDetail>)
    }

    const renderImg_4 = () => {
        return (<ImageDetail>
            <div style={{ width: '50%', height: 'inherit'}}>
                <div style={{ height: '50%', border: '0.1px solid white' }}>
                    <ImgCover src={images[0]} />
                </div>
                <div style={{ height: '50%', border: '0.1px solid white' }}>
                    <ImgCover src={images[1]}/>
                </div>
            </div>
            <div style={{ width: '50%', height: 'inherit'}}>
                <div style={{ height: '50%', border: '0.1px solid white' }}>
                    <ImgCover src={images[2]}/>
                </div>
                <div style={{ height: '50%', border: '0.1px solid white' }}>
                    <ImgCover src={images[3]}/>
                </div>
            </div>
        </ImageDetail>)
    }

    const renderImg_5 = (morethan5 = false, images_total = 0) => {
        return (<ImageDetail>
            <div style={{ width: '50%', height: 'inherit'}}>
                <div style={{ height: '50%', border: '0.1px solid white' }}>
                    <ImgCover src={images[0]} />
                </div>
                <div style={{ height: '50%', border: '0.1px solid white' }}>
                    <ImgCover src={images[1]}/>
                </div>
            </div>
            <div style={{ width: '50%', height: 'inherit'}}>
                <div style={{ height: '33%', border: '0.1px solid white' }}>
                    <ImgCover src={images[2]}/>
                </div>
                <div style={{ height: '33%', border: '0.1px solid white' }}>
                    <ImgCover src={images[3]}/>
                </div>
                <div style={{ height: '34%', border: '0.1px solid white', position: 'relative' }}>
                    <ImgCover src={images[4]}/>
                    { morethan5 && <MoreImages> {images_total - 5}+ </MoreImages> }
                </div>
            </div>
        </ImageDetail>)
    }


    const renderCondition = () => {
        let imgComp = ''
        switch(images.length){
            case 0: break;
            case 1: imgComp = renderImg_1(); break;
            case 2: imgComp = renderImg_2(); break;
            case 3: imgComp = renderImg_3(); break;
            case 4: imgComp = renderImg_4(); break;
            case 5: imgComp = renderImg_5(); break;
            default: break;
        }

        if( images.length > 5 ){
            imgComp = renderImg_5(true, images.length)
        }

        return imgComp;
    }

    return ( renderCondition() )
}