import React, { useState, useEffect } from "react"
import { Link } from "gatsby"

import "react-image-gallery/styles/css/image-gallery.css";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import loginAction from "../../../reducers/login/actions";
import Layout from "../../layout";

import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/swiper-bundle.css';
import { DetailCard } from "../card";

const l1 = { title: 'Hot Deal', list: [1,2,3,4,5,6] }
const l2 = { title: 'Suggestion', list: [1,2,3,4,5,6] }
const l3 = { title: 'Lastest Post', list: [1,2,3,4,5,6] }

const HomeMobilePage = (props) => {

    const [load, setLoad] = useState(false)


    useEffect( () => {
      window.addEventListener('scroll', handleScroll);
      return () => window.removeEventListener('scroll', handleScroll);
    }, [])

    useEffect(() => {
      if( load ){
        props.loadPost()
      }

    }, [load])


    const createTopicSwiper = ({ title='', list =[] }) => {
        return (
          <>
            {
              list.map( l => <DetailCard detail={l} goToPostDetail={props.goToPostDetail}/> )
            }
            {/*
            <Swiper
                spaceBetween={16}
                slidesPerView={1}
                //onSlideChange={() => console.log('slide change')}
                onSwiper={(swiper) => console.log(swiper)}
                style={{ height: '45vh', width: '96%' }}
                onSlideChange={ handleSlideChange }
                //centeredSlides={true}
            >
                {
                  list.map( l => <SwiperSlide><DetailCard detail={l} goToPostDetail={props.goToPostDetail}/></SwiperSlide> )
                }
            </Swiper>
            */}
          </>
        )
    }

    const handleSlideChange = (swiper) =>{
      console.log('onSliderMove', swiper.activeIndex, swiper.isEnd, swiper.realIndex )
      if( swiper.slides && swiper.activeIndex === swiper.slides.length - 2 ){
        console.log('load', props.currentPage)
        //props.loadPost()
        setLoad(true)
      }
    }

    const handleScroll = () => {
      const scrollTop = (document.documentElement
        && document.documentElement.scrollTop)
        || document.body.scrollTop;
      const scrollHeight = (document.documentElement
        && document.documentElement.scrollHeight)
        || document.body.scrollHeight;
      if (scrollTop + window.innerHeight + 50 >= scrollHeight){
        setLoad(true);
      }
    }

    return (
        <>
          <div style={{ margin: '16px 0', paddingBottom: '16px' }} >
            { createTopicSwiper({ title: 'Test', list: props.postList || [] }) }
          </div>
        </>
    )
}


function mapStateToProps(state) {
  return {

  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({

  }, dispatch)
}

let HomeMobilePageRedux = connect(
  mapStateToProps,
  mapDispatchToProps
)(HomeMobilePage);

export default HomeMobilePageRedux
