/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.com/docs/use-static-query/
 */

import React, { useEffect, useState, useRef } from "react"
import PropTypes from "prop-types"
import { useStaticQuery, graphql, navigate } from "gatsby"
import styled from 'styled-components'
import "./layout.css"
import { LoginMobile } from "./login/mobile"
import queryString from 'query-string'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import loginAction from "../reducers/login/actions"
import { BottomNavMobile } from "./utils/BottomNav/mobile"
import { TopNavMobile } from "./utils/TopNav/mobile"
import { UserTopNavMobile } from "./utils/UserTopNav/mobile"
import GraphAPI from "../services/graphql"
import { getUserProfile, getNotification } from "../services/gql-query"
import { UserManager } from "oidc-client"
import { config } from "../services/login-config"
import { Avatar } from "antd"
import { PlusOutlined } from "@ant-design/icons"
import { globalHistory } from "@reach/router/lib/history"
import { useCallback } from "react"

const Container = styled.div`
  max-width: 1024px;
  margin: 0 auto;
`

const Layout = ({ 
  children, 
  showLoginModal, setLoginModal , 
  is_login, setLogin, 
  setUserProflie, setUserNotification, setNotificationInfo,
  topNavFixed
}) => {
  
  const data = useStaticQuery(graphql`
    query SiteTitleQuery {
      site {
        siteMetadata {
          title
        }
      }
    }
  `)

  const [windowSize, setWindowSize] = useState({
    width: undefined,
    height: undefined,
  });

  const [showCreatePost, setShowCreatePost] = useState(false)
  const [event, setEvent] = useState(undefined)

  const [prevScrollpos, setPrevScrollpos] = useState(0);
  //const [login, setLoginModal] = useState(true)

  //const URL = process.env.GATSBY_RABBIT_FORGOT_PW_URL
  useEffect(() => {

    // Add event listener
    //window.addEventListener("resize", handleResize);
    //window.addEventListener("scroll", handleScroll);

    // Call handler right away so state gets updated with initial window size
    handleResize();
    setupEvent();
    console.log('window.location.pathname', window.location.pathname)
    setShowCreatePost( window.location.pathname === '/home/' )

    eventListeners.current = handleScroll;

    // Remove event listener on cleanup
    return () => {
      event()
      //window.removeEventListener("resize", handleResize);
      //window.removeEventListener("scroll", handleScroll);
    }
    
  }, []); // Empty array ensures that effect is only run on mount

  useEffect(() => {
    const userId = localStorage.getItem('userId')
    const tokenId = localStorage.getItem('token')
    console.log( `userId =======>`, userId )
    console.log( `tokenId =======>`, tokenId )

    if( userId || tokenId){
      setLogin(true)
      getUserProfile()
      getNotification()
    }
  }, [ typeof window !== 'undefined' && (localStorage.getItem('userId') || localStorage.getItem('token')) ] )

  const eventListeners = useRef();

  useEffect( () => {

    if( topNavFixed ){
      document.getElementsByClassName("navbar")[0].style.top = "0";
      window.removeEventListener("scroll", eventListeners.current, true)
    }else{
      window.addEventListener("scroll", eventListeners.current, true)
    }
  }, [topNavFixed] )

  const handleScroll = useCallback((event) => {
    var currentScrollPos = window.pageYOffset;

    if (prevScrollpos >= currentScrollPos) {
      document.getElementsByClassName("navbar")[0].style.top = "0";
    } else {
      document.getElementsByClassName("navbar")[0].style.top = "-140px";
    }
    setPrevScrollpos(currentScrollPos);
  })


  const getUserProfile = async () => {
    const { data } = await GraphAPI.getMyProfileRequest()
    console.log( `data`, data.data.me.edge )
    setUserProflie( data.data.me.edge )
  }

  const getNotification = async () => {
    const { data } = await GraphAPI.getNotificationRequest()
    console.log( `data`, data.data.userNotiList )
    setUserNotification( data.data.userNotiList.edges )
    setNotificationInfo( data.data.userNotiList.totalCount, data.data.userNotiList.totalUnRead )
  }

  const setupEvent = () => {
    globalHistory.listen(({ location }) => {
      console.log( 'globalHistory', location )
      const params = queryString.parse(location.search)
      const { login } = params
      setLoginModal( login ? true : false )
      
      setShowCreatePost( location.pathname === '/home' )
    })
    console.log('inttttt')

    //setEvent(event)
  }

  const processLogin = () => {
    return <LoginMobile 
      visible={showLoginModal} 
      handleCancel={() => setLoginModal(false)}
      login={ () => loginProcess() } 
      handleOk={() => {
        setLogin(true)
        setLoginModal(false)
      }}
    />
  }

  const processNavBar = () => {
    return (
      is_login && <BottomNavMobile />
    )
  }

  const processTopNavBar = () => {
    //const isLogin = false
    return (
      is_login ? <UserTopNavMobile/> : <TopNavMobile setLoginModal={setLoginModal} /> 
    )
  }

  const loginProcess = async () => {
    let userManager = new UserManager(config);
    let user = await userManager.getUser();
    if (user) {
      console.log( 'user', user )
      localStorage.setItem('token', user.id_token)
    } else {
      await userManager.signinRedirect();
      //await userManager.signinPopup();
    }
  }

  
  const handleResize = () => {
    // Set window width/height to state
    setWindowSize({
      width: window.innerWidth,
      height: window.innerHeight,
    });

    //window.matchMedia("(max-width: 1023px)").matches
  }

  return (
    <Container>
      <div className={'navbar'}>
        { processTopNavBar() }
        { processNavBar() }
      </div>
      <div style={{ paddingTop: is_login ? '120px': '56px' }}>
        {children}
      </div>
      { processLogin() }
      {
        showCreatePost &&
        <div style={{ position: 'fixed', right: '3vw', bottom: '3vw' }}>
          <Avatar size={44} icon={<PlusOutlined/>} style={{ background: '#61d195' }} onClick={ () => navigate('/post-create') } />
        </div>
      }
    </Container>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

function mapStateToProps(state) {
  return {
    showLoginModal : state.login.showLoginModal,
    is_login: state.login.is_login,
    topNavFixed: state.topNav.fixedNavAll
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    setLoginModal: loginAction.functions.setLoginModal,
    setLogin: loginAction.functions.setLogin,
    setUserProflie: loginAction.functions.setUserProflie,
    setUserNotification: loginAction.functions.setUserNotification,
    setNotificationInfo: loginAction.functions.setNotificationInfo
  }, dispatch)
}

let LayoutRedux = connect(
  mapStateToProps,
  mapDispatchToProps
)(Layout);

export default LayoutRedux
