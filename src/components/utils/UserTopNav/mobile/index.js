import React, { useEffect, useState } from "react"
import styled from 'styled-components'
import { Carousel, Layout, Menu, Input, Card, Avatar, Timeline, Button, Badge, Popover } from 'antd';
import { UserOutlined, BellOutlined, SearchOutlined } from "@ant-design/icons";
import { navigate, navigateTo } from "gatsby";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import './nav.css'
import GraphAPI from "../../../../services/graphql";

const { Header, Footer, Sider, Content } = Layout;

const Container = (props) => {

    return (
        <Header style={{ background: '#61d195', display: 'flex', alignItems: 'center', padding: '0 16px', justifyContent: 'space-between' }}>
            <div></div>
            <div style={{ display: 'flex', alignItems: 'center' }}>
                <div> <Avatar size={32} icon={ <SearchOutlined /> } /></div>
                <div style={{ margin: '8px' }} ></div>
                <Avatar size={32} icon={ props.user_profile.profileImage ? <img src={props.user_profile.profileImage} /> : <UserOutlined />} onClick={()=> navigate('/profile')} />
            </div>
        </Header>
    )
}

function mapStateToProps(state) {
    return {
        user_profile: state.login.user_profile,
        notifications: state.login.notifications,
        totalNotification: state.login.totalNotification,
        totalUnReadNotification: state.login.totalUnReadNotification
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        
    }, dispatch)
}

export const UserTopNavMobile = connect(
    mapStateToProps,
    mapDispatchToProps
)(Container);

//export default UserTopNavMobile