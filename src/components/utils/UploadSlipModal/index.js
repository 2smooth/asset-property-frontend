import React, { useEffect, useState } from "react"
import { Modal } from "antd"
import styled from 'styled-components'
import { PaymentContainer } from "../../../pages/post-payment"

export const UploadSlipModal = ({ 
    isModalVisible = false,
    handleOk,
    handleCancel,
    order = {},
    processAfterUploadSlipComplete
}) => {
    return (
        <Modal 
            title="Upload Slip" 
            visible={isModalVisible} 
            onOk={handleOk} 
            onCancel={handleCancel}
            centered={true}
            footer={null}
        >
            { order.postId }
            <br/>
            { order.orderId }
            <PaymentContainer orderId={order.orderId} processAfterUploadSlipComplete={processAfterUploadSlipComplete} />
        </Modal>
    )
}