import React, { useEffect, useState } from "react"
import styled from 'styled-components'
import { Carousel, Layout, Menu, Input, Card, Avatar, Timeline, Button } from 'antd';
import { HomeOutlined } from "@ant-design/icons";
import { navigateTo } from "gatsby";
const { Header, Footer, Sider, Content } = Layout;

const Container = styled.div`
    background-color: #61d195;
    position: fixed;
    bottom: 0;
    height: 56px;
    width: 100%;
    display: flex;
    justify-content: space-around;
    align-items: center;
    justify-content: flex-end;
`

export const TopNavMobile = ({ 
    setLoginModal
}) => {
    return (
        <Header style={{ background: '#61d195', display: 'flex', alignItems: 'center', padding: '0 16px', justifyContent: 'space-between' }}>
            <HomeOutlined style={{ fontSize: '30px' }} onClick={ () => navigateTo('/home') } />
            <Button onClick={ () => setLoginModal(true) }> Login / Signup </Button>
        </Header>
    )
}