import React, { useEffect, useState } from "react"
import styled from 'styled-components'
import { navigateTo, push, navigate } from "gatsby"
import { HomeOutlined, ShoppingCartOutlined, SearchOutlined, FormOutlined, BellOutlined } from "@ant-design/icons"
import { Modal, Popover, Badge, Avatar } from "antd"
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import GraphAPI from "../../../../services/graphql"

const Container = styled.div`
    background-color: #61d195;
    /*
    position: fixed;
    top: 0;
    */
    height: 56px;
    width: 100%;
    max-width: 1024px;
    display: flex;
    justify-content: space-around;
    align-items: center;
    z-index: 1;
    text-align: center;
`

const BottomNavMobileComponent = (props) => {

    
    const [ showNotification, setShowNotification ] = useState(false)

    useEffect( () => {
        //document.body.style.overflow =  showNotification ? 'hidden' : ''
    }, [showNotification])

    const createNotification = () => {
        const { notifications, totalUnReadNotification } = props
        console.log( 'notifications', totalUnReadNotification )
        return (
            <Popover
                content={createNotificationContent()}
                //title="Title"
                trigger="click"
                visible={showNotification}
                placement="bottom" 
                onVisibleChange={ visible => setShowNotification(visible)}
            >
                 <Badge count={totalUnReadNotification} style={{ right: '-8px' }}>
                    <BellOutlined />
                </Badge>
                <br/>
                Notification
            </Popover>
        )
    }

    const createNotificationContent = () => {
        const { notifications = [] } = props
        return (
            <>
                { notifications != null && notifications.map( noti => {
                    return ( 
                        <div 
                            style={{ background: noti.isRead ? 'unset': '#e6e6e6', borderBottom: '1px solid gray' }}
                            onClick={ () => stampReadNotiAndGoToOrderPage(noti.notiId, noti.orderId) }
                        >
                            <div style={{padding: '8px' }} >{noti.textTh}</div> 
                        </div>
                    )
                } ) }
            </>
        )
    }

    const stampReadNotiAndGoToOrderPage = async (notiId, orderId) => {
        const { data } = await GraphAPI.stampReadNotificationRequest(notiId)
        console.log( `stampReadNotiAndGoToOrderPage`, data )
        setShowNotification(false)
        navigate(`/my-order?order_id=${orderId}`)
    }

    const redirectPage = ( location ) => {
        navigate(location)
    }

    const commingSoon = () => {
        Modal.info({
            content: 'Comming Soon'
        })
    }

    return (
        <Container>
            <div onClick={ () => redirectPage('/home') }><HomeOutlined /><br/>Home</div>
            <div onClick={ () => redirectPage('/my-order') }><ShoppingCartOutlined /><br/>My Order</div>
            <div> {createNotification()} </div>
            {/*<div onClick={ () => redirectPage('/post-create') }><FormOutlined /><br/>Create Post</div>*/}        </Container>
    )
}


function mapStateToProps(state) {
    return {
        user_profile: state.login.user_profile,
        notifications: state.login.notifications,
        totalNotification: state.login.totalNotification,
        totalUnReadNotification: state.login.totalUnReadNotification
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        
    }, dispatch)
}

export const BottomNavMobile = connect(
    mapStateToProps,
    mapDispatchToProps
)(BottomNavMobileComponent);