import { actions } from "./actions";

const initialState = {
    fixedNavAll: false
}

export default (state = initialState, action) => {
    const { type, payload } = action;
    switch (type) {
        case actions.SET_FIXED_TOP_NAV_ALL:
            console.log( `actions.SET_FIXED_TOP_NAV_ALL`, payload )
            return { ...state, fixedNavAll: payload };
        default: 
            return state;
    }
}