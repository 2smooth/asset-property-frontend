const actions = {
    SET_FIXED_TOP_NAV_ALL: 'SET_FIXED_TOP_NAV_ALL',
    SET_FIXED_TOP_NAV_FIRST: 'SET_FIXED_TOP_NAV_FIRST',
    SET_FIXED_TOP_NAV_SECOND: 'SET_FIXED_TOP_NAV_SECOND',
}

const functions = {
    setFixedTopNavAll: (isFixed=false) => ({ type: actions.SET_FIXED_TOP_NAV_ALL, payload: isFixed })
}

const topNavAction = {
    actions, 
    functions
}

export default topNavAction
export {
    actions, 
    functions
}
