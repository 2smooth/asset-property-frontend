const actions = {
    SET_CART : 'SET_CART',
    SET_POST: 'SET_POST'
}

const functions = {
    setProductsToCart: (products= []) => ({ type: actions.SET_CART, payload: products }),
    setPost: (post={}) => ({ type: actions.SET_POST, payload: post }),
}

const cartAction = {
    actions, 
    functions
}

export default cartAction
export {
    actions, 
    functions
}