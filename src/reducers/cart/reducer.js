import { actions } from "./actions";

const initialState = {
    post: undefined,
    products: [],
}

export default (state = initialState, action) => {
    const { type, payload } = action;
    switch (type) {
        case actions.SET_CART:
            return { ...state, products: payload };
        case actions.SET_POST:
            return { ...state, post: payload };
        default: 
            return state;
    }
}