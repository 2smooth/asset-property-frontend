/* global window:true */
/* eslint no-underscore-dangle: 0 */

import { createStore, applyMiddleware, compose  } from 'redux';
//import createSagaMiddleware from 'redux-saga';
import rootReducer from './reducers';
//import rootSaga from './sagas';

//const sagaMiddleware = createSagaMiddleware();
//const middlewares = [sagaMiddleware];

export default () => {
  const devtools =
    process.env.NODE_ENV === 'development' && window.devToolsExtension
      ? window.__REDUX_DEVTOOLS_EXTENSION__ &&
        window.__REDUX_DEVTOOLS_EXTENSION__()
      : f => f;

  const store = createStore(rootReducer, 
    compose(
       devtools
    )
  );
  //sagaMiddleware.run(rootSaga);

  if (module.hot) {
    // Enable Webpack hot module replacement for reducers
    module.hot.accept('./reducers', () => {
      const nextRootReducer = require('./reducers');
      store.replaceReducer(nextRootReducer);
    });
  }

  return store;
};
