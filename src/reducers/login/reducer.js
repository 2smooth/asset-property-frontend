import { actions } from "./actions";

const initialState = {
    showLoginModal: false,
    is_login: false,
    user_profile: {},
    notifications: [],
    totalNotification: 0,
    totalUnReadNotification: 0
}

export default (state = initialState, action) => {
    const { type, payload } = action;
    switch (type) {
        case actions.SET_LOGIN_MODAL:
            console.log( 'SET_LOGIN_MODAL', payload )
            return { ...state, showLoginModal: payload };
        case actions.SET_IS_LOGIN: 
            return { ...state, is_login: payload };
        case actions.SET_USER_PROFILE:
            return { ...state, user_profile: payload }
        case actions.SET_NOTIFICATION:
            return { ...state, notifications: payload }
        case actions.SET_NOTIFICATION_INFO:
            return { ...state, 
                totalNotification: payload.totalUnReadNotification, 
                totalUnReadNotification: payload.totalUnReadNotification }
        default: 
            return state;
    }
}