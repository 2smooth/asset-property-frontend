const actions = {
    SET_LOGIN_MODAL : 'SET_LOGIN_MODAL',
    SET_IS_LOGIN : 'SET_IS_LOGIN',
    SET_USER_PROFILE: 'SET_USER_PROFILE',
    SET_NOTIFICATION: 'SET_NOTIFICATION',
    SET_NOTIFICATION_INFO: 'SET_NOTIFICATION_INFO'
}

const functions = {
    setLoginModal: (show_login = false) => ({ type: actions.SET_LOGIN_MODAL, payload: show_login }),
    setLogin: (login = false) =>  ({ type: actions.SET_IS_LOGIN, payload: login }),
    setUserProflie: (profile={}) => ({ type: actions.SET_USER_PROFILE, payload: profile }),
    setUserNotification: (notifications=[]) => ({ type: actions.SET_NOTIFICATION, payload: notifications }),
    setNotificationInfo: (totalNoti, totalUnRead) => ({ type: actions.SET_NOTIFICATION_INFO, payload: {totalNotification: totalNoti, totalUnReadNotification: totalUnRead} })
}

const loginAction = {
    actions, 
    functions
}

export default loginAction
export {
    actions, 
    functions
}
