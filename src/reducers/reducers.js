import { combineReducers } from 'redux';
import login from './login/reducer'
import cart from './cart/reducer'
import topNav from './topnav/reducer'

export default combineReducers({ 
    login,
    cart,
    topNav
});
