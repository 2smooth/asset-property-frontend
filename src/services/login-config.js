
export const config = {
    client_id: process.env.GATSBY_CLIENT_ID,
    redirect_uri: process.env.GATSBY_REDIRECT_URL + '/callback',
    post_logout_redirect_uri: process.env.GATSBY_REDIRECT_URL + '/logout',
    silent_redirect_uri: process.env.GATSBY_REDIRECT_URL + '/silent-callback',

    response_type: 'code',
    scope: 'openid offline',
    authority: 'https://auth.gohew.com',
    automaticSilentRenew: true,
    loadUserInfo: false,
}