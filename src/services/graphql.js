import Axios from 'axios';
import { getOTP, getCustomerSubscriptions, verifyOTP, confirmPayment, cancelSubscription, getPostList, uploadImg, getPostById, createPost, createOrder, getUserOrder, uploadSlip, confirmSlipCorrect, confirmOrderDelivery, getUserProfile, getNotification, setReadNotification, getMyProfile } from './gql-query'

//console.log( `process.env.GATSBY_RECURRING_URL`, process.env.GATSBY_RECURRING_URL )
//const apiPath = process.env.GATSBY_RECURRING_URL //"https://9n34uk0fuk.execute-api.ap-southeast-1.amazonaws.com/default/ascend-donate-api"
const apiPath = 'https://api.gohew.com/graphql'

class APIService {
    requestHeader() {
        const token = localStorage.getItem("token");
        const token_debug = localStorage.getItem("userId");
        return {
            "Content-Type": "application/json",
            "Accept": "application/json",
            "Authorization": token ? `Bearer ${token}` : '',
            "Authorization-debug": token_debug ? `{ "userId": "${token_debug}" }` : ''
        };
    }

    request(query, variables, uploadFile= false) {
        let headers = this.requestHeader();
        let data = undefined

        if( uploadFile ){
            headers['mimeType'] = "multipart/form-data"

            data = new FormData();
            data.append('operations', '{"query":"mutation singleUpload($file:Upload!) {singleUpload(file: $file){\\nfilename\\nmimetype\\nencoding\\nurl}}"}');
            data.append('map', '{"0": ["variables.file"]}');
            data.append('0', variables.file);
        }else{
            data = JSON.stringify({
                query: query,
                variables: JSON.stringify(variables),
            })
        }
        // console.log(headers);
        //console.log( query, JSON.stringify(variables) )
        for( let key in variables ){
            //query = query.replace(`_$${key}`, '"'+variables[key] + '"')
        }

        return Axios({
            method: 'POST',
            url: apiPath, //+ '/graphql',
            headers: headers,
            data: data
            /*
            validateStatus: (status) => {
                return true; // I'm always returning true, you may want to do it depending on the status received
            },
            */
        })
        .then(response => {
            //console.log(response);
            return Promise.resolve(response);
        })
        .catch(error => {
            console.log(error.response);
            return Promise.reject(error.response);
        })
    }

    getPostListRequest(offset=0, limit=10){
        return this.request(getPostList, { offset: parseInt(offset)* limit, limit })
    }

    getPostByIdRequest(id){
        return this.request(getPostById, { id })
    }

    uploadImgRequest(file){
        return this.request(uploadImg, { file }, true)
    }

    createPostRequest({ title,
        description,
        orderCloseAt,
        deliveryAt,
        deliveryLocation,
        imageUrls,
        postItems }){
            return this.request(createPost, { title,
                description,
                orderCloseAt,
                deliveryAt,
                deliveryLocation,
                imageUrls,
                postItems })
    }

    createOrderRequest({ postId, orderItems, deliveryLat, deliveryLng, userContactInfo, deliveryRemark }){
        return this.request(createOrder, { postId, orderItems, deliveryLat, deliveryLng, userContactInfo, deliveryRemark })
    }

    getUserOrderRequest({confirmStatus='', offset = 0, limit= 10} = {}){
        console.log()
        let cs = confirmStatus ? `$confirmStatus: OrderConfirmStatus!,` : ``
        let cs_value = confirmStatus ? `confirmStatus: $confirmStatus` : ``
        let query = getUserOrder.replace(`$status`, cs)
                    .replace(`$status_value`, cs_value)
        
        let variable = { confirmStatus, offset: offset * limit, limit }
        if( !confirmStatus ) delete variable.confirmStatus
        return this.request(query, variable)
    }

    uploadSlipRequest({ orderId, slipUrl }){
        return this.request(uploadSlip, { orderId, slipUrl })
    }

    confirmSlipCorrectRequest({ orderId }){
        console.log( confirmSlipCorrect, orderId )
        return this.request(confirmSlipCorrect, {orderId})
    }

    confirmOrderDeliveryRequest({ orderId, shippingCode }){
        return this.request(confirmOrderDelivery, {orderId, shippingCode})
    }

    getUserProfileRequest(id){
        //const id = localStorage.getItem("userId")
        return this.request(getUserProfile, { id })
    }

    getNotificationRequest(offset = 0 ){
        return this.request(getNotification, { offset })
    }

    stampReadNotificationRequest(id){
        return this.request(setReadNotification, { id })
    }

    getMyProfileRequest(){
        return this.request(getMyProfile)
    }
    /*
    verifyOTPRequest({ session_token, confirm_token, otp_ref, otp_code, auth_code }){
        return this.request(verifyOTP, {
            session_token, confirm_token, otp_ref, otp_code, auth_code
        })
    }

    confirmPaymentRequest({ session_token, confirm_token }){
        return this.request(confirmPayment, {
            session_token, confirm_token
        })
    }

    getCustomerSubscriptionsRequest({ session_token }){
        return this.request(getCustomerSubscriptions, {
            session_token
        })
    }
    cancelSubscriptionRequest({ session_token, subscription_id }){
        return this.request(cancelSubscription, {
            session_token, subscription_id
        })
    }
    */
}

export const GraphAPI = new APIService()
export default GraphAPI
