const getPostList = `
query PostList($offset : Int!, $limit: Int!){
  postList(
    input:{
    	 pageInput:{
        limit: $limit
        offset: $offset
      }
      filter:{
      	
      }
      sorter:{
        createdAt:{
          order: DESC
          priority: 1
        }
      }
    }
  ){
    edges{
      postId
      userId
      titleEn
      titleTh
      descEn
      descTh
      imageUrls
      orderCloseAt
      deliveryAt
      deliveryLocation
      deliveryLat
      deliveryLng
      deliveryArea
      createdAt
      updatedAt
			
      user{
        userId
        profileName
        profileImage
      }
    }
    pageInfo{
      currentPage
    }
    totalCount
  }
}
`
const getPostById = `
query Post($id : ID!){
  post(
    input:{
    	id: $id
    }
  ){
    edge{
      postId
      userId
      titleEn
      titleTh
      descEn
      descTh
      imageUrls
      orderCloseAt
      deliveryAt
      deliveryLocation
      deliveryLat
      deliveryLng
      deliveryPrice
      deliveryArea
      createdAt
      updatedAt
			
      user{
        userId
        profileName
        profileImage
      }
      postItems{
        postId
        postItemId
        nameEn
        nameTh
        price
        imageUrl
      }
    }
  }
}
`

const uploadImg = `
mutation singleUpload($file:Upload!) {
  singleUpload(file: $file){
    filename
    mimetype
    encoding
    url
  }
}
`

const createPost = `
mutation createPost(
  $title: String!, 
  $description: String!, 
  $orderCloseAt: DateTime!, 
  $deliveryAt: DateTime!, 
  $deliveryLocation: String!, 
  $deliveryPrice: Int!,
  $imageUrls: String!,
  $postItems: [PostItemCreateByPostInput]
){
  createPost( 
    input:{
      titleEn: $title,
      titleTh: $title,
      descEn: $description,
      descTh: $description,
      imageUrls: $imageUrls,
      orderCloseAt: $orderCloseAt,
      deliveryAt: $deliveryAt,
      deliveryLocation: $deliveryLocation
      deliveryPrice: $deliveryPrice
      postItems: $postItems
    }
  ){
    msg
    status
    data{
      postId
      userId
      titleEn
      titleTh
      descEn
      descTh
      imageUrls
      orderCloseAt
      deliveryAt
      postItems{
        nameEn
        nameTh
        price
        imageUrl
      }
    }
  }
}
`

const createOrder = `
mutation CreateOrder(
  $postId: ID!, 
  $orderItems: [OrderItemCreateByOrderInput] 
  $deliveryLat: String!
  $deliveryLng: String!
  $userContactInfo: String!
  $deliveryRemark: String!
){
  createOrder(input:{
    postId: $postId
    orderItems: $orderItems
    deliveryLat: $deliveryLat
    deliveryLng: $deliveryLng
    userContactInfo: $userContactInfo
    deliveryRemark: $deliveryRemark
  }){
    msg
    status
    data{
      orderId
    }
  }
}
`

const getUserOrder = `
query MyOrderList($status $limit: Int!, $offset: Int!){
  myOrderList(input:{
    filter:{
      $status_value
    }
    pageInput:{
      limit: $limit
      offset: $offset
    }
    sorter:{
      createdAt:{
        order: DESC
        priority: 0
      }
    }
  }){
    edges{
      orderId
      userId
      postId
      postUserId
      totalQuantity
      totalItemPrice
      deliveryPrice
      totalPrice
      deliveryLat
      deliveryLng
      deliveryRemark
      slipUrl
      confirmStatus
      shippingCode
      orderItems{
        orderItemId
        userId
        postItemNameTh
        price
        quantity
      }
      post{
        titleEn
        titleTh
        imageUrls
      }
      user{
        userId
        profileName
      }
      postUser{
        userId
        profileName
      }
    }
    totalCount
    totalWaiting
    totalPending
    totalConfirmed
    totalDeliverying
    totalCompleted
    totalCanceled
    pageInfo{
      currentPage
    }
  }
}
`

const uploadSlip = `
mutation OrderUploadSlip( $orderId: ID!, $slipUrl: String! ){
  orderUploadSlip(input:{
    orderId: $orderId
    slipUrl: $slipUrl
  }){
    msg
    status
  }
}
`

const confirmSlipCorrect = `
mutation OrderShopConfirm( $orderId: ID! ){
  orderShopConfirm(input:{
    orderId: $orderId
  }){
    msg
    status
  }
}
`

const confirmOrderDelivery = `
mutation OrderShopDelivery( $orderId: ID!, $shippingCode: String! ){
  orderShopDelivery(input:{
    orderId: $orderId
    shippingCode: $shippingCode
  }){
    msg
    status
  }
}
`

const getUserProfile = `
query User( $id: ID! ){
  user(input:{
  	id: $id
  }){
    edge{
    	userId
      username
      firstName
      lastName
      profileName
      profileImage
    }
  }
}
`

const getNotification = `
query UserNotiList( $offset: Int! ){
  userNotiList(input:{
    pageInput:{
      limit: 10
      offset: $offset
    }
    sorter:{
      createdAt:{
        order: DESC
        priority: 1
      }
    }
  }){
    edges{
      notiId
      textEn
      textTh
      orderId
      postId
      isRead
      createdAt
    }
    totalCount
    totalUnRead
  }
}
`

const setReadNotification = `
mutation readUserNoti( $id: ID! ){
  readUserNoti(input:{
  	lastUserNotiId: $id
  }){
    msg
    status
  }
}
`

const getMyProfile = `
query{
  me{
    edge{
      userId
      username
      firstName
      lastName
      profileName
      profileImage
      tel
      email
      isVerified
      shopTel
      shopLineId
      shopFacebookUrl
      shopTextTemplate_1
  	}
	}
}
`

export {
  getPostList,
  uploadImg,
  getPostById,
  createPost,
  createOrder,
  getUserOrder,
  uploadSlip,
  confirmSlipCorrect,
  confirmOrderDelivery,
  getUserProfile,
  getNotification,
  setReadNotification,
  getMyProfile
}