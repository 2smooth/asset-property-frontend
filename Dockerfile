FROM node:12-buster as build
MAINTAINER José Moreira <josemoreiravarzim@gmail.com>
RUN yarn global add gatsby-cli
WORKDIR /app
ADD . ./
RUN yarn config set network-timeout 600000 -g
RUN yarn
RUN ACTIVE_ENV=production gatsby build
RUN ls

FROM gatsbyjs/gatsby
COPY --from=build /app/public /pub
